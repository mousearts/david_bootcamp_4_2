<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Student;

class StudentController extends Controller
{
    function addStudent(Request $request){
        DB::beginTransaction();
        try{

            $this->validate($request, [
                'student_name' => 'required',
                'address' => 'required',
                'phone_number' => 'required'
            ]);

            $student_name = $request -> input('student_name');
            $address = $request -> input('address');
            $phone = $request -> input ('phone_number');

            $student = App\Student::updateOrCreate(

            ['student_name' => $student_name],
            ['student_address' => $address, $student=>$phone]);
           

            

            DB::commit(); //commit untuk masuk kedalam database bila berhasil
            return response()->json(["message" => "Success"], 201);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
            
        }
    }
}
