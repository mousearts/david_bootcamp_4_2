import { Component, OnInit, Input } from '@angular/core';
import { CourseService } from '../../services/course.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  course_code: string = '';
  course_name: string = '';
  course_price: number;
  

  @Input('course') courseList: Object [];

  constructor(private course:CourseService) { }

  ngOnInit() {
    this.courseList = this.course.getCourseList();
  }



  addCourse(){
    this.course.addCourse({
      "course_code": this.course_code,
      "course_name":this.course_name,
      "course_price":this.course_price,
            

    });
  }

  removeCourse(course_code){
    this.courseList = this.course.removeBookList(course_code);
  }

}
