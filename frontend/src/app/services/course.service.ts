import { Injectable } from '@angular/core';

@Injectable()
export class CourseService {

  constructor() { }

  courseList : Object [] = [
    {
      "id" : 1,
      "course_code" : "ABC11",
      "course_name" : "Programming Fundamental",
      "course_price" : 2000000,
    },
    {
      "id" : 2,
      "course_code" : "ABC12",
      "course_name" : "Object Oriented Programming",
      "course_price" : 3000000,
    },
    {
      "id" : 3,
      "course_code" : "ABC13",
      "course_name" : "Database Design",
      "course_price" : 4000000,
    },
    {
      "id" : 4,
      "course_code" : "ABC14",
      "course_name" : "Front End Development",
      "course_price" : 8000000,
    },
    {
      "id" : 5,
      "course_code" : "ABC15",
      "course_name" : "Back End Development",
      "course_price" : 10000000,
    },
  ];

  getCourseList():object[]
  {
    return this.courseList;
  }

  addCourse(obj:object):object[]
  {
    this.courseList.push(obj);
    return this.courseList;
  
  }

  removeBookList(course_code:string):object[]
  {
    for (var i = 0; i < this.courseList.length; i++) {
      if (this.courseList[i]["course_code"] == course_code) {
        this.courseList.splice(i, 1);
        break;
      } 
    }
    return this.courseList;
  }

}
